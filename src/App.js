import React, { useState, useEffect } from 'react'
import { Card } from './components/Card';
import { fb, crud } from './utils/firebase';




const App = () => {
  const [todos, setTodos] = useState([])
  const [done, setDone] = useState([])
  const [attr, setAttr] = useState({
    dueDate: "",
    todo: "",
    key: ""
  })


  useEffect(() => {
    fb.database().ref("Todo").on('value', snapShot => {
      if (snapShot.val())
        setTodos(
          Object.keys(snapShot.val()).map(key => ({
            ...snapShot.val()[key],
            key
          }))
        ) 
      else setTodos([])
    })
  })

  const onHandlerInput = ({ target: { value, name } }) => {
    setAttr({
      ...attr,
      [name]: value
    })
  }

  const handleCreateTodo = () => {
    crud.createTodo(attr)
    setAttr({
      dueDate: "",
      todo: "",
      key: ""
    })
  }

  // const hancleCreateDone = (data) => {
  //   crud.createDone(data)
  // }

  const handleCancel = () => {
    setAttr({
      dueDate: "",
      todo: "",
      key: ""
    })
  }

  const del = key => {
    todos.map((data, index) => {
      if (data.key == key) {
        setTodos(todos.splice(index, 1))
      }
    })
    crud.del(key)
  }

  return (
    <div className="container">
      <div className="section" >
        <div className="box">
          <textarea className="textarea" placeholder="type something..." name="todo" onChange={onHandlerInput} value={attr.todo}></textarea>
          <input type="date" name="dueDate" onChange={onHandlerInput} />
          <a class="button is-danger" style={{
            marginLeft: "85%",
            display: "inline"
          }} onClick={handleCancel} >Cancel</a>
          <a class="button is-success" style={{
            marginLeft: "1%",
            display: "inline"
          }}
            onClick={handleCreateTodo}
          >Submit</a>
        </div>
      </div>
      <br />
      <div className="section">
        <div className="columns">
          <div className="column">
            {
              todos.map(data => <Card data={data} key={data.todo} del={del} />)
            }

          </div>
          <div className="column">
          {
              done.map(data => <Card data={data} key={data.todo}/>)
          }
          </div>
        </div>
      </div>
    </div>
  )
}

export default App