import React from 'react'

const Card = ({data,del}) => <div className="card" style={{backgroundColor:""}}>
    <header className="card-header">
        <p className="card-header-title">
            Component
  </p>
        <a href="#" className="card-header-icon" aria-label="more options">
            <span className="icon">
                <i className="fas fa-angle-down" aria-hidden="true"></i>
            </span>
        </a>
    </header>
    <div className="card-content">
        <div className="content">
        {data.todo} 
            <br />
            <time dateTime="2016-1-1">{data.dueDate}</time>
        </div>
    </div>
    <footer className="card-footer">
        <a href="#" className="card-footer-item">Save</a>
        <a href="#" className="card-footer-item">Edit</a>
        <a href="#" className="card-footer-item" onClick={() => del(data.key)}>Delete</a>
    </footer>
</div>

Card.defaultProps ={
    data: {
        todo:"",
        dueDate:""
    }
}

export {
    Card
}
