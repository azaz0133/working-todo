import firebase from 'firebase'

const fb = firebase.initializeApp(
    {
        apiKey: "AIzaSyAH8C-oIiCtwsAWz1DL3zUDimi2V0Kzwy8",
        authDomain: "todowork-8e85f.firebaseapp.com",
        databaseURL: "https://todowork-8e85f.firebaseio.com",
        projectId: "todowork-8e85f",
        storageBucket: "todowork-8e85f.appspot.com",
        messagingSenderId: "272701986604"
    }
)


const db = fb.database()

export const crud = {

    createTodo: attrs => new Promise((resolve, reject) => {
        db.ref("Todo").push({
            ...attrs
        }).then(() => resolve("created")).catch(err => reject(err))
    }),

    createDone: attrs => new Promise((resolve, reject) => {
        db.ref("Done").push({
            ...attrs
        }).then(() => resolve("created")).catch(err => reject(err))
    }),

    del: key => new Promise((resolve, reject) => {
         db.ref("Todo").child(key).remove().then(() => resolve("deleted"))
    })

}

export {
    fb
}
